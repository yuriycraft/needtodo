//
//  AboutViewController.h
//  НужноСделать.рф
//
//  Created by book on 14.02.16.
//  Copyright © 2016 yuriy.craft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)buttonAction:(id)sender;
- (IBAction)barButtonAction:(id)sender;

@end
