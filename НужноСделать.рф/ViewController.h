//
//  ViewController.h
//  НужноСделать.рф
//
//  Created by apple on 29.01.16.
//  Copyright © 2016 yuriy.craft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelTopSpaseToLogoConstraint;

- (IBAction)buttonAction:(id)sender;


@end

