//
//  WelcomeView.h
//  НужноСделать.рф
//
//  Created by apple on 29.01.16.
//  Copyright © 2016 yuriy.craft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeView : UIView
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelTopContraint;
@property (weak, nonatomic) IBOutlet UIButton *button;
- (IBAction)buttonAction:(id)sender;

@end
