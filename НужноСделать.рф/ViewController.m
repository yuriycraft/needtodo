//
//  ViewController.m
//  НужноСделать.рф
//
//  Created by apple on 29.01.16.
//  Copyright © 2016 yuriy.craft. All rights reserved.
//

#import "ViewController.h"
#import "WelcomeView.h"
#import "SCLAlertView.h"
#import <AFNetworking.h>
#define IS_IPHONE_4                                                            \
  (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)480) <    \
   DBL_EPSILON)
#define IS_IPHONE_5                                                            \
  (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)568) <    \
   DBL_EPSILON)
#define IS_IPHONE_6                                                            \
  (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)667) <    \
   DBL_EPSILON)
#define IS_IPHONE_6p                                                           \
  (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)736) <    \
   DBL_EPSILON)

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *telephoneTextField;
@property (weak, nonatomic) IBOutlet UITextView *toDoTextView;
@property(weak, nonatomic) WelcomeView *welcomView;
@property(weak ,nonatomic) SCLAlertView *alert;
@end

@implementation ViewController

- (void)viewDidLoad {
    
  [super viewDidLoad];
  [self showWelcomeView];
    
    self.toDoTextView.layer.borderWidth = 0.6f;
    self.toDoTextView.layer.cornerRadius = 3.0f;
    self.toDoTextView.layer.borderColor = [UIColor colorWithRed:207.f/255.f green:216.f/255.f blue:220.f/255.f alpha:1.f].CGColor;
    self.nameTextField.layer.borderColor = [UIColor colorWithRed:207.f/255.f green:216.f/255.f blue:220.f/255.f alpha:1.f].CGColor;
       self.telephoneTextField.layer.borderColor = [UIColor colorWithRed:207.f/255.f green:216.f/255.f blue:220.f/255.f alpha:1.f].CGColor;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    UITapGestureRecognizer *gestureRecognizer =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hideKeyboard)];
    [self.scrollView addGestureRecognizer:gestureRecognizer];
    gestureRecognizer.cancelsTouchesInView = NO;
  // Do any additional setup after loading the view, typically from a nib.
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Отмена" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Готово" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _telephoneTextField.inputAccessoryView = numberToolbar;
}

- (void)viewWillLayoutSubviews {
    
  [super viewWillLayoutSubviews];

  if (IS_IPHONE_6p) {
      
    //_labelTopSpaseToLogoConstraint.constant = 80;
  //  _scrollView.contentInset = UIEdgeInsetsMake(80, 0, 0, 0);

      
  } else if (IS_IPHONE_6) {
      
   // _labelTopSpaseToLogoConstraint.constant = 30.0;
   // _scrollView.contentInset = UIEdgeInsetsMake(30.0, 0, 0, 0);
      
  }

  _scrollView.contentSize = CGSizeMake(_contentView.bounds.size.width, 460.f);
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
  [aScrollView setContentOffset:CGPointMake(0, aScrollView.contentOffset.y)];
}

- (void)keyboardWasShown:(NSNotification *)notification
{
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        // Step 1: Get the size of the keyboard.
        CGFloat keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
        
        // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardHeight+50, 0.0);
        
        _scrollView.contentInset = contentInsets;
        
       _scrollView.scrollIndicatorInsets = contentInsets;
        
        _scrollView.scrollEnabled=YES;
    }
}

- (void) keyboardWillHide:(NSNotification *)notification {
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
   _scrollView.contentInset = contentInsets;
    
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    [_scrollView setContentOffset:CGPointMake(0.0f, 0.0f) animated:TRUE];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _scrollView.contentSize.height);
    
    _scrollView.scrollEnabled=NO;
    
}

#pragma mark - set scrollView content position

-(void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat theViewY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] bounds];
    CGFloat avaliableHeight = applicationFrame.size.height - 200;
    CGFloat y = theViewY - avaliableHeight / 2.0;
    if(y<0)
        y = 0;
    [_scrollView setContentOffset:CGPointMake(0,y) animated:YES];
}

#pragma -mark UITextFieldDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self scrollViewToCenterOfScreen:textField];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == _nameTextField) {
        [textField resignFirstResponder];
        [_telephoneTextField becomeFirstResponder];
    } else if (textField == _telephoneTextField) {
        [_toDoTextView resignFirstResponder];
    }
    return YES;
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    [self.view endEditing:YES];
    
}


#pragma mark UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [self scrollViewToCenterOfScreen:textView];
    return YES;
}

- (BOOL)textViewShouldReturn:(UITextView *)textView {
    
    [self.view endEditing:YES];
    
    return NO;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    
    [self.view endEditing:YES];
    
    return YES;
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }else{
        return YES;
    }
}

- (void)hideKeyboard {
    
    [self.scrollView.window endEditing:YES];
    
}


- (void)showWelcomeView {

  _welcomView =
      [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([WelcomeView class])
                                    owner:self
                                  options:nil][0];

  _welcomView.frame = self.view.bounds;
  
    if (IS_IPHONE_4) {
      
    _welcomView.logoTopConstraint.constant = 20.f;
    _welcomView.labelTopContraint.constant = 20;
    _welcomView.buttonTopConstraint.constant = 10.f;
  
  }
    
  UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;

  [currentWindow addSubview:_welcomView];
  self.view.window.windowLevel = UIWindowLevelStatusBar;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)showThanksViewWithString:(NSString*)string {

SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert showCustom:self image:[UIImage imageNamed:@"master-1"]
                color:[UIColor colorWithRed:0.f/255.f green:173.f/255.f blue:234.f/255.f alpha:1.f] title:@"Ваша заявка успешно отправлена!" subTitle:string closeButtonTitle:@"Спасибо!" duration:0.0f];
 
}
- (void)showError {
    
    SCLAlertView *alertError = [[SCLAlertView alloc] init];
    
    [alertError showCustom:self image:[UIImage imageNamed:@"master-1"]
                color:[UIColor redColor] title:@"Не все поля заполнены!" subTitle:@"" closeButtonTitle:@"ОК" duration:0.0f];
    
}

- (IBAction)buttonAction:(id)sender {
    
    if (_nameTextField.text.length<3  || !_toDoTextView.text.length || _telephoneTextField.text.length<6) {
      
        [self showError];
    
    } else {

        NSString *name = [NSString stringWithFormat:@"%@",_nameTextField.text];
         NSString *phone = [NSString stringWithFormat:@"%@",_telephoneTextField.text];
         NSString *todo = [NSString stringWithFormat:@"%@",_toDoTextView.text];
        
        
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:name,@"name",phone ,@"phone",todo,@"task",nil];
       // NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:name,@"uname",phone ,@"uphone",todo,@"descr",nil];
        NSLog(@"%@",params);
        

   //    NSURL *URL = [NSURL URLWithString:@"http://xn--80ahdc1aibi1aoc1j.xn--p1ai/orenburg/one_click_project" ];
  /*      AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
         manager.responseSerializer = [AFHTTPResponseSerializer serializer];
       // [manager.requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Accept"];
        [manager.requestSerializer setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4" forHTTPHeaderField:@"User-Agent"];
       [manager.requestSerializer setValue:@"application/x-www-form-urlencoded; charset=UTF8" forHTTPHeaderField:@"Content-Type"];
        
        [manager POST:@"http://xn--80ahdc1aibi1aoc1j.xn--p1ai/orenburg/one_click_project/"  parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"success!");
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error: %@", error);
        }]; */
//
//        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//       manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//      //  AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//
//
//            //  manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
//       //  [manager.requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Content-Type"];
//        [manager.requestSerializer setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4" forHTTPHeaderField:@"User-Agent"];
//        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//       // manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
//        [manager POST:@"http://xn--80ahdc1aibi1aoc1j.xn--p1ai/orenburg/one_click_project" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//            NSLog(@"success!");
//        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSLog(@"error: %@", error);
//        }];
//        
//       
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];

        [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
         [manager.requestSerializer setValue:@"text/html" forHTTPHeaderField:@"Accept"];
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
        [manager.requestSerializer setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4" forHTTPHeaderField:@"User-Agent"];
        
        [manager POST:@"http://xn--80ahdc1aibi1aoc1j.xn--p1ai/orenburg/one_click_project" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"success!");
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error: %@", error);
        }];
    }
}


    
-(void)cancelNumberPad{
    [_telephoneTextField resignFirstResponder];
    _telephoneTextField.text = @"";
}

-(void)doneWithNumberPad{
    if (_toDoTextView.text.length > 0) {
          [_telephoneTextField resignFirstResponder];
    }
    else {
        [_toDoTextView becomeFirstResponder];
    }

}
@end
