//
//  AboutViewController.m
//  НужноСделать.рф
//
//  Created by book on 14.02.16.
//  Copyright © 2016 yuriy.craft. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
  //  _scrollView.contentSize = self.view.bounds.size;
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {

    [aScrollView setContentOffset:CGPointMake(0, aScrollView.contentOffset.y)];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)buttonAction:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];

}

- (IBAction)barButtonAction:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
